from .builder import *

def run(file_name, ignore_modules=[], ignore_functions=[]):
	file = open(file_name)

	builder = Builder()
	builder.ignore_modules = ignore_modules
	builder.ignore_functions = ignore_functions

	tokens = Lexer.process(file)
	builder.process(tokens)

	file.close()

	json = builder.compile()
	file = open('diagram.gliffy', 'w')
	file.write(json)
	file.close()

if __name__ == "__main__":
	run("test.seq")
