
class Lexer(object):
	@staticmethod
	def process(file):
		tokens = []
		for line in file:
			if line == "" or line.startswith("#"):
				continue

			if not line.startswith("(<"):
				continue

			pid_end_index = line.find(">)")
			pid = line[1:pid_end_index + 1]

			call_index = line.find(">) call")
			if call_index != -1:
				mod_index = line.find("(", call_index)
				mod_fun = line[call_index + 8:mod_index]
				parts = mod_fun.split(':')
				token = Token('call', parts[0], parts[1], pid)
				tokens.append(token)
			else:
				ret_index = line.find(">) return")
				if ret_index != -1:
					mod_index = line.find("/")
					mod_fun = line[ret_index + 17:mod_index]
					parts = mod_fun.split(':')
					token = Token('ret', parts[0], parts[1], pid)
					if (tokens[-1].type, tokens[-1].module, tokens[-1].function) == \
							('call', token.module, token.function):
						tokens[-1].type = 'call_ret'
					else:
						tokens.append(token)

		return tokens


class Token(object):
	def __init__(this, t, m, f, p):
		this.type = t
		this.module = m
		this.function = f
		this.pid = p

	def __str__(this):
		return this.type + " " + this.module + ":" + this.function

class Stack(object):
	def __init__(this):
		this.frames = []

	@property
	def top(this):
		return this.frames[-1]

	def add(this, frame):
		this.frames.insert(0, frame)

	def push(this, frame):
		this.frames.append(frame)

	def pop(this):
		frame = this.frames.pop()

		return frame

class Frame(object):
	def __init__(this, source, dest, function):
		this.pid = None
		this.source = source
		this.dest = dest
		this.function = function

	def __str__(this):
		return this.source + "->" + this.dest + ":" + this.function


class SequenceFrame(Frame):
	def __init__(this, frame, type):
		super(SequenceFrame, this).__init__(frame.source, frame.dest, frame.function)
		this.pid = frame.pid
		this.type = type
		this.async = False
		this.circular = False

	def __str__(this):
		return super(SequenceFrame, this).__str__() + " (" + this.type + ", " + str(this.async) + ")"
