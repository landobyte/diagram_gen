import gliffy

from .processing import *

class Builder(object):
	def __init__(this):
		this.gliffy_main = gliffy.GliffyMain()
		this.stack = Stack()
		this.async_stack = Stack()
		this.modules = ["SPAWN"]
		this.call_tree = {}
		this.ignore_modules = []
		this.ignore_functions = []
		this.sequence = []

		this.current_source = "SPAWN"
		this.current_pid = ""

		this.column_width = 160
		this.column_spacing = 10
		this.row_height = 30

	def process(this, tokens):
		this.current_pid = None
		for token in tokens:
			if token.module not in this.ignore_modules and token.function not in this.ignore_functions:
				this.process_token(token)
		this.build_diagram()

	def process_token(this, token):
		print(token)
		if token.type.startswith('call'):
			frame = Frame(this.current_source, token.module, token.function)

			if token.pid != this.current_pid:
				frame.source = "SPAWN"
				this.current_pid = frame.pid = token.pid

			seq = SequenceFrame(frame, token.type)

			current_source_index = this.modules.index(this.current_source)
			if token.module not in this.modules:
				this.modules.append(token.module)
				this.call_tree[token.module] = [this.current_source]
			elif this.modules.index(token.module) < current_source_index:
				dest_index = this.modules.index(token.module)
				circular = False
				for caller in this.call_tree[this.current_source]:
					if this.modules.index(caller) >= dest_index:
						print("CIRCULAR DEP")
						circular = True
						break
				if circular:
					seq.circular = True
				else:
					this.modules.insert(dest_index, this.modules.pop(current_source_index))

			if this.current_source not in this.call_tree[token.module]:
				this.call_tree[token.module].append(this.current_source)

			if token.type == 'call':
				this.stack.push(frame)
				this.current_source = token.module
			this.sequence.append(seq)
		elif token.type == 'ret':
			async_ret = False
			for frame in this.async_stack.frames:
				if (frame.dest, frame.function) == (token.module, token.function):
					async_ret = True
					this.async_stack.frames.remove(frame)
					seq = SequenceFrame(frame, token.type)
					seq.async = True
					this.sequence.append(seq)
					break

			if not async_ret:
				while (this.stack.top.dest, this.stack.top.function) != (token.module, token.function):
					frame = this.stack.pop()
					this.async_stack.add(frame)
					for seq in reversed(this.sequence):
						if (seq.dest, seq.function) == (frame.dest, frame.function):
							seq.async = True
							break
					this.current_source = frame.dest

				frame = this.stack.pop()
				seq = SequenceFrame(frame, token.type)
				this.sequence.append(seq)
				this.current_source = frame.source
		else:
			raise SyntaxError("Unknown token '" + token.type + "'")

	def build_dependency_tree(this):
		for seq in this.sequence:
			pass

	def build_diagram(this):
		module_column = 0
		for module in this.modules:
			module_obj = gliffy.GliffyProcessObject(module)
			module_obj.y = 20
			module_obj.width = this.column_width
			module_obj.height = 30
			module_obj.column_spacing = 10
			module_obj.column = module_column
			module_obj.tags['FILLCOLOR'] = "#0B599F"
			module_obj.child = gliffy.GliffyText(module, "#FFF", "160")
			module_obj.fit_child()
			module_column += 1
			this.gliffy_main.objs.append(module_obj)

			points = u"[{0}, 40], [{0}, {1}]".format(
				str(module_obj.x + (this.column_width / 2)),
				str(module_obj.y + module_obj.height + len(this.sequence) * this.row_height + 20))
			line_obj = gliffy.GliffyLine(points=points)
			this.gliffy_main.objs.append(line_obj)

		seq_index = 0
		for seq in this.sequence:
			seq_index += 1
			source_index = this.modules.index(seq.source)
			dest_index = this.modules.index(seq.dest)
			start_x = 10 + (this.column_width / 2)
			total_column_width = this.column_width + this.column_spacing * 2

			points = u"[{0}, {2}], [{1}, {2}]".format(
				str(start_x + total_column_width * source_index),
				str(start_x + total_column_width * dest_index),
				str(40 + seq_index * this.row_height))
			line_obj = gliffy.GliffyLine(points=points)

			if seq.type == 'call':
				line_obj.end_arrow = "2"
			elif seq.type == 'call_ret':
				line_obj.start_arrow = "3"
				line_obj.end_arrow = "2"
			elif seq.type == 'ret':
				line_obj.start_arrow = "3"

			if seq.circular:
				line_obj.color = "#990000"

			this.gliffy_main.objs.append(line_obj)

			if seq.type.startswith("call"):
				function_obj = gliffy.GliffyProcessObject(seq.function)
				if not seq.circular:
					function_obj.x = start_x + total_column_width * dest_index + 5
				else:
					function_obj.x = start_x + total_column_width * dest_index - (this.column_width + 5)
					function_obj.tags['FILLCOLOR'] = "#B3474D"
				function_obj.y = 40 + seq_index * this.row_height - 10
				function_obj.width = this.column_width
				function_obj.height = 20
				function_obj.text_line_height = 15
				function_obj.fit_child()
				this.gliffy_main.objs.append(function_obj)

				if seq.pid:
					pid_obj = gliffy.GliffyProcessObject(seq.pid)
					pid_obj.x = start_x + total_column_width * source_index + 20
					pid_obj.y = 40 + seq_index * this.row_height - 10
					pid_obj.width = 80
					pid_obj.child.tags["W"] = "80"
					pid_obj.height = 20
					this.gliffy_main.objs.append(pid_obj)
			else:
				ret_obj = gliffy.GliffyText(seq.function, "#777", "160")
				ret_obj.x = start_x + total_column_width * source_index + 20
				ret_obj.y = 40 + seq_index * this.row_height - 20
				ret_obj.h_align = "left"
				this.gliffy_main.objs.append(ret_obj)

	# def add_call_to(this, m, f):
	# 	pass
	#
	# def add_ret_to(this, m, f):
	# 	this.gliffy_main.objs.append()
	#
	# def add_call_ret_to(this, m, f):
	# 	pass

	def compile(this):
		return this.gliffy_main.build()
