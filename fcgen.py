#! /usr/bin/python

import sys
import fc


if len(sys.argv) > 1:
	print("Running " + sys.argv[1])

	fc.main.FILENAME = sys.argv[1]
	fc.main.run()
else:
	print("Running debug...")
	fc.main.run()
