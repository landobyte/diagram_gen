import os

class TemplateLoader(object):
	objects = {
		'main': None,
		'object': None,
		'line': None,
		'text': None,
		'paragraph': None,
		'line_constraints': None
	}

	@classmethod
	def get_template(cls, obj_type):
		if not cls.objects[obj_type]:
			cls.objects[obj_type] = cls.load(obj_type)
		return cls.objects[obj_type]

	@staticmethod
	def load(obj_type):
		f = open(os.path.dirname(os.path.realpath(__file__)) + "/templates/" + obj_type + ".tmpl")
		template = f.read()
		f.close()
		return template
