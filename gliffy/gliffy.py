from .base import *


class GliffyNodeObject(GliffyObject):
	def __init__(this, obj_type):
		super(GliffyNodeObject, this).__init__(obj_type=obj_type)
		this.tags = {'UID': "com.gliffy.shape.flowchart.flowchart_v1.default.start_end",
					 'TID': "com.gliffy.stencil.start_end.flowchart_v1",
					 'FILLCOLOR': "#0B599F"}
		this.width = 100
		this.height = 40

		label = ""
		if obj_type == 'start':	label = "Start"
		elif obj_type == 'end': label = "End"
		elif obj_type == 'error': label = "Error"
		this.child = GliffyText(label,"#FFF", "50", overflow="both")


class GliffyProcessObject(GliffyObject):
	def __init__(this, description):
		super(GliffyProcessObject, this).__init__(obj_type='process')
		this.tags = {'UID': "com.gliffy.shape.flowchart.flowchart_v1.default.process",
					 'TID': "com.gliffy.stencil.rectangle.basic_v1",
					 'FILLCOLOR': "#DDE6F1"}
		this.width = 160
		this.height = 40
		this.child = GliffyText(description,"#000","160")
		this.fit_child()


class GliffyIoObject(GliffyObject):
	def __init__(this, text):
		super(GliffyIoObject, this).__init__(obj_type='io')
		this.tags = {'UID': "com.gliffy.shape.flowchart.flowchart_v1.default.input_output",
					 'TID': "com.gliffy.stencil.rhombus.basic_v1",
					 'FILLCOLOR': "#424D76"}
		this.width = 160
		this.height = 60
		this.child = GliffyText(text,"#FFF","160")
		this.fit_child()


class GliffyIfObject(GliffyObject):
	def __init__(this, expression):
		super(GliffyIfObject, this).__init__(obj_type='decision')
		this.tags = {'UID': "com.gliffy.shape.flowchart.flowchart_v1.default.decision",
					 'TID': "com.gliffy.stencil.diamond.basic_v1",
					 'FILLCOLOR': "#0B599F"}
		this.width = 160
		this.height = 100
		this.child = GliffyText(expression,"#FFF","160")
		this.fit_child()


class GliffyLine(GliffyContainerBase):
	def __init__(this, points="[0,0], [0,0]", label=None):
		super(GliffyLine, this).__init__(obj_type='line')
		this.tags = {'CONSTRAINTS': "", 'COLOR': "#0B599F", 'S_ARW': "0", 'E_ARW': "0", 'LINEPOINTS': points}
		this.color = "#0B599F"
		this.start_arrow = "0"
		this.end_arrow = "0"

		if label:
			this.child = GliffyText(label, "#000", "30", overflow="both")

	def build(this):
		this.tags['COLOR'] = this.color
		this.tags['S_ARW'] = this.start_arrow
		this.tags['E_ARW'] = this.end_arrow

		return super(GliffyLine, this).build()


class GliffyConnectorLine(GliffyLine):
	def __init__(this, source_obj, dest_obj, label=None):
		super(GliffyConnectorLine, this).__init__(label=label)
		this.constraints = GliffyLineContraints(source_obj, dest_obj)
		this.tags['E_ARW'] = "2"

		if not label and source_obj.port_label:
			this.child = GliffyText(source_obj.port_label, "#000", "30", overflow="both")

	def build(this):
		this.tags['CONSTRAINTS'] = this.constraints.build()

		return super(GliffyConnectorLine, this).build()


class GliffyLineContraints(GliffyBase):
	def __init__(this, source_obj, dest_obj):
		super(GliffyLineContraints, this).__init__(obj_type='line_constraints')
		this.tags = {'S_ID': source_obj.id, 'E_ID': dest_obj.id}

		if source_obj.out_port == 'bottom':
			this.tags.update({'S_PX': "0.5", 'S_PY': "1.0"})
		elif source_obj.out_port == 'right':
			this.tags.update({'S_PX': "1.0", 'S_PY': "0.5"})

		if dest_obj.in_port == 'top':
			this.tags.update({'E_PX': "0.5", 'E_PY': "0.0"})
		elif dest_obj.in_port == 'left':
			this.tags.update({'E_PX': "0.0", 'E_PY': "0.5"})


class GliffyText(GliffyBase):
	def __init__(this, text, color, width, overflow="none"):
		super(GliffyText, this).__init__(obj_type='text')
		this.tags = {'X': 0, 'Y': 0, 'W': width, 'OVERFLOW': overflow, 'HTML': ""}
		this.lines = this.make_lines(text)
		this.x = 0
		this.y = 0
		this.color = color
		this.h_align = "center"

	@staticmethod
	def make_lines(text):
		lines = []
		if len(text) <= 22:
			lines = [text]
		else:
			break_points = "~_-:(, "
			result = (0,20)
			i = 0
			while len(text) > 22:
				char = text[i]
				i += 1
				if i % 22 == 0 or i == len(text):
					lines.append(text[:result[1]])
					text = text[result[1]:]
					result = (0,20)
					i = 0
				elif char in break_points:
					v = (i % 22) * break_points.index(char)
					if result[0] < v:
						result = (v, i)
			if len(text) > 0:
				lines.append(text)

		return lines

	def build(this):
		this.tags['X'] = str(this.x)
		this.tags['Y'] = str(this.y)

		json = ""
		for line in this.lines:
			paragraph = GliffyParagraph(line, this.color)
			paragraph.tags['HALIGN'] = this.h_align
			json += paragraph.build()
		this.tags['HTML'] = json

		return super(GliffyText, this).build()


class GliffyParagraph(GliffyBase):
	def __init__(this, text, color):
		super(GliffyParagraph, this).__init__(obj_type='paragraph')
		this.text = text
		this.tags = {'TEXT': text, 'COLOR': color, 'HALIGN': "center"}
