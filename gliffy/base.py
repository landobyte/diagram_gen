from .template_loader import *

class GliffySuper(object):
	def __init__(this, obj_type='super'):
		this.type = obj_type
		this.tags = {}

	def load_template(this):
		return TemplateLoader.get_template(this.type)

	def build(this):
		json = this.load_template()
		for key in this.tags:
			value = this.tags[key]
			json = json.replace("&" + key, value)

		return json

	def __str__(this):
		return this.type

class GliffyMain(GliffySuper):
	def __init__(this, title=""):
		super(GliffyMain, this).__init__(obj_type='main')
		this.tags = {'TITLE': title, 'OBJECTS': ""}
		this.objs = []

	def build(this):
		obj_json = ""
		for obj in this.objs:
			if not obj_json:
				obj_json += obj.build()
			else:
				obj_json += ",\n" + obj.build()

		this.tags['OBJECTS'] = obj_json

		return super(GliffyMain, this).build()

class GliffyBase(GliffySuper):
	base_id = 200

	def __init__(this, id=None, obj_type='base'):
		super(GliffyBase, this).__init__(obj_type=obj_type)
		this.id = id if id else this.new_id()

	@staticmethod
	def new_id():
		obj_id = GliffyBase.base_id
		GliffyBase.base_id += 1

		return str(obj_id)

	def build(this):
		this.tags['ID'] = this.id

		return super(GliffyBase, this).build()

	def __str__(this):
		return this.id + ":" + this.type

class GliffyContainerBase(GliffyBase):
	def __init__(this, id=None, obj_type='container'):
		super(GliffyContainerBase, this).__init__(id=id, obj_type=obj_type)
		this._column = 0
		this.column_width = 160
		this.column_spacing = 10
		this.x = 20
		this.y = 20
		this.width = 160
		this.height = 40
		this.text_line_height = 20
		this.child = None

	@property
	def column(this):
		return this._column
	@column.setter
	def column(this, value):
		this._column = value
		this.x = this.column_spacing + this._column * (this.column_width + this.column_spacing * 2) + \
				 ((this.column_width - this.width) / 2)

	def fit_child(this):
		# if type(this.child) is GliffyText:
		# Assume type(this.child) is GliffyText
		num_lines = len(this.child.lines)
		if num_lines > 1:
			this.height = num_lines * this.text_line_height

	def build(this):
		this.tags['X'] = str(this.x)
		this.tags['Y'] = str(this.y)
		this.tags['W'] = str(this.width)
		this.tags['H'] = str(this.height)

		child_json = ""
		if this.child:
			child_json = this.child.build()
		this.tags['CHILDREN'] = child_json

		return super(GliffyContainerBase, this).build()

	def __str__(this):
		text = this.child.text if this.child else ""
		return super(GliffyContainerBase, this).__str__() + " [" + str(this._column) + "," + str(
			this.y) + "] {" + text + "}"

class GliffyObject(GliffyContainerBase):
	obj_id = 0

	def __init__(this, obj_type):
		super(GliffyObject, this).__init__(id=this.new_id(), obj_type=obj_type)
		this.in_port = 'top'
		this.out_port = 'bottom'
		this.port_label = None

	@staticmethod
	def new_id():
		obj_id = GliffyObject.obj_id
		GliffyObject.obj_id += 1

		return str(obj_id)

	def load_template(this):
		return TemplateLoader.get_template('object')
