#! /usr/bin/python

import sys
from seq import main


if len(sys.argv) > 1:
	print("Running " + sys.argv[1])

	ignore_modules = []
	if len(sys.argv) > 2:
		ignore_modules = sys.argv[2].split(" ")

	ignore_functions = []
	if len(sys.argv) > 3:
		ignore_functions = sys.argv[3].split(" ")

	main.run(sys.argv[1], ignore_modules, ignore_functions)
else:
	main.run("test.seq")
