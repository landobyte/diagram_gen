import cgi

class Lexer(object):
	@staticmethod
	def process(line):
		parts = line.split(':', 1)
		token_set = TokenSet(parts[0])
		if len(parts) > 1:
			token_set.text = cgi.escape(parts[1], True)

		return token_set


class TokenSet(object):
	def __init__(this, type):
		this.type = type
		this.text = None


class Stack(object):
	def __init__(this):
		this.frames = [Frame()]
		this.popped = False

	@property
	def top(this):
		return this.frames[-1]

	def push(this, root):
		frame = Frame(root)
		frame.column = this.top.column
		this.frames.append(frame)

	def update(this, root):
		this.top.root = root
		this.top.objs = [root]

	def pop(this):
		frame = this.frames.pop()
		this.top.objs = frame.obj_stack
		column_inc = frame.column - this.top.column
		if column_inc > this.top.column_inc:
			this.top.column_inc = column_inc
		this.popped = True


class Frame(object):
	def __init__(this, root=None):
		this.root = root
		this.objs = [root]
		this.obj_stack = []
		this.column = 0
		this.column_inc = 1
		this.has_else = False

	def push(this):
		this.obj_stack += this.objs
		this.root.out_port = 'right'
		this.root.port_label = 'False'
		this.objs = [this.root]
		this.column += this.column_inc
		this.column_inc = 1

	def update(this, obj):
		this.objs = [obj]



