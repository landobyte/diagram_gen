import gliffy

from .processing import *

class Builder(object):
	def __init__(this):
		this.gliffy_main = gliffy.GliffyMain()
		this.stack = Stack()
		this.advance_y = True

	def process(this, tokens):
		if tokens.type == 'start':
			this.add_start()

		elif tokens.type == 'process' or tokens.type == 'proc' or tokens.type == 'block':
			this.add_object(gliffy.GliffyProcessObject(tokens.text))
		elif tokens.type == 'io':
			this.add_object(gliffy.GliffyIoObject(tokens.text))

		elif tokens.type == 'if':
			obj = gliffy.GliffyIfObject(tokens.text)
			obj.port_label = "True"
			this.add_object(obj)
			this.stack.push(obj)
		elif tokens.type == 'elif' or tokens.type == 'elseif':
			this.stack.top.push()
			this.advance_y = False
			obj = gliffy.GliffyIfObject(tokens.text)
			obj.port_label = "True"
			obj.in_port = 'left'
			this.add_object(obj)
			this.stack.update(obj)
		elif tokens.type == 'else':
			this.stack.top.has_else = True
			this.stack.top.push()
		elif tokens.type == 'endif':
			if not this.stack.top.has_else:
				this.stack.top.push()
			this.stack.top.push()
			this.stack.pop()

		elif tokens.type == 'end':
			this.add_object(gliffy.GliffyNodeObject(tokens.type))
		elif tokens.type == 'error':
			this.add_object(gliffy.GliffyNodeObject(tokens.type))
		else:
			raise SyntaxError("Unknown token '"+ tokens.type + "'")

	def add_start(this):
		obj = gliffy.GliffyNodeObject('start')
		this.gliffy_main.objs.append(obj)
		this.stack.top.update(obj)

	def add_object(this, obj):
		obj._column = this.stack.top._column
		if this.advance_y:
			obj.y = this.next_y()
		else:
			obj.y = this.stack.top.objs[0].y
			this.advance_y = True
		this.gliffy_main.objs.append(obj)
		this.add_lines_to(obj)
		this.stack.top.update(obj)

	def add_lines_to(this, obj):
		for o in this.stack.top.objs:
			if o.type != 'error':
				line_obj = gliffy.GliffyConnectorLine(o, obj)
				this.gliffy_main.objs.append(line_obj)

	def next_y(this):
		max_y = 0
		for o in this.stack.top.objs:
			o_y = o.y + o.height
			if o_y > max_y:
				max_y = o_y
		next_y = max_y + 20

		if this.stack.popped:
			this.stack.popped = False
			next_y += 20

		return next_y

	def compile(this):
		return this.gliffy_main.build()
