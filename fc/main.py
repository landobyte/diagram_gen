import sys

from .builder import *

FILENAME = "test.fc"

def run():
	file = open(FILENAME)

	builder = Builder()

	for line in file:
		line = line.strip()
		if line == "" or line[0] == '#':
			continue

		print(line)
		tokens = Lexer.process(line)
		builder.process(tokens)

	file.close()

	json = builder.compile()
	file = open('diagram.gliffy', 'w')
	file.write(json)
	file.close()


if __name__ == "__main__":
	run()
